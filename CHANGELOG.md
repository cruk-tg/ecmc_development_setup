ecmc_development_setup CHANGELOG
================================

This file is used to list changes made in each version of the ecmc_development_setup cookbook.
0.1.7
-----
- Add vagrant to docker group

0.1.6
-----
- Add git flow

0.1.5
-----
- Update repositories

0.1.4
-----
- Add nodejs via NODEjs repo

0.1.2
-----
- Add vagrant to rbenv ruby group

0.1.1
-----
- Add rbenv ruby 2.1.3 to setup

0.1.0
-----
- Paul D Mitchell - Initial release of ecmc_development_setup

- - -
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.
