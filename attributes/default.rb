default['rbenv']['user_installs'] =[
  {
    'user' => 'vagrant'
  }
]
default['java']['install_flavor'] = 'oracle'
default['java']['jdk_version'] = 8
default['java']['accept_license_agreement'] = true
default['java']['oracle']['accept_oracle_download_terms'] = true
default['ruby']['version'] = '2.5'
default['nodejs']['nodesource'] = 'https://deb.nodesource.com/node_10.x'
