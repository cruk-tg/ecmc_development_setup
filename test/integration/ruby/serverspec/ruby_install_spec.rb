require 'serverspec'

set :backend, :exec

describe command('ruby -v') do
  its(:stdout) { should match '2.5.1' }
end

describe file('/etc/profile.d/Z99-ruby-gems.sh') do
  it { should be_file }
end

describe iptables do
  it { should have_rule('-A ufw-user-input -p tcp -m tcp --dport 3000 -j ACCEPT')}
end
