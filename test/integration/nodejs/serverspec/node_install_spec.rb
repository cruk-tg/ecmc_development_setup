require 'serverspec'

set :backend, :exec

describe command('nodejs --version') do
  its(:stdout) { should match /10\.\d+\.\d+/ }
end

describe command('npm --version') do
  its(:stdout) { should match /5\.\d+\.\d+/ }
end

describe package('yarn') do
  it { should be_installed }
end
