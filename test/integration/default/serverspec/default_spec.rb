require 'serverspec'

set :backend, :exec

describe file('/usr/local/share/ca-certificates/university_of_edinburgh_ca_2.crt') do
  it { should be_file }
end

describe file('/etc/ssl/certs/c0a7f8f0.0') do
  it { should be_file }
end

describe command('git version') do
  its(:stdout) { should match(/git version \d+\.\d+\.\d+/)}
end

describe command('docker-compose --version') do
  its(:stdout) { should match(/docker-compose version \d+\.\d+\.\d+/)}
end
