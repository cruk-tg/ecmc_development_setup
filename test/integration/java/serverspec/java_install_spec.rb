require 'serverspec'

set :backend, :exec
set :path, '/usr/local/rbenv/shims:/usr/local/rbenv/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/home/vagrant/.rbenv/shims'

describe command('java -version') do
  its(:stderr) { should match '1.8.0' }
end
