name             'ecmc_development_setup'
maintainer       'Paul D Mitchell'
maintainer_email 'paul.d.mitchell@ed.ac.uk'
license          'All rights reserved'
description      'Installs/Configures ecmc_development_setup'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '2.2.0'

depends 'apt'
depends 'firewall'
depends 'java'
depends 'hostsfile'

supports 'ubuntu'
