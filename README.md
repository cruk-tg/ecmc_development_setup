ecmc_development_setup Cookbook
===============================

Requires the firewall cookbook

Provides:

* SSH port 22
* SSH port 3000 (webrick)
* University of Edinburgh CA certificates

Provides recipes for
* JAVA (default 8)
* RUBY (default 2.5)
* NODEJS (default 10)

Also installs docker-compose
Adds docker and git completion
Adds git prompt

To add ruby use ecmc_development_setup::ruby_install recipe
To add Java use ecmc_development_setup::java_install recipe
To add Nodejs use ecmc_development_setup::nodejs_install recipe

Version: 2.2.0

