#
# Cookbook Name:: ecmc_development_setup
# Recipe:: java_install
#
# Copyright 2017, University of Edinburgh
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'java'
