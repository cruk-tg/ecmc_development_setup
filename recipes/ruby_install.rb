#
# Cookbook Name:: ecmc_development_setup
# Recipe:: ruby_install
#
# Copyright 2017, University of Edinburgh
#
# All rights reserved - Do Not Redistribute
#
apt_repository 'ruby' do
  uri 'ppa:brightbox/ruby-ng'
  distribution node['lsb']['codename']
end

apt_package 'ruby' do
  package_name "ruby#{node['ruby']['version']}"
end

apt_package 'ruby-dev' do
  package_name "ruby#{node['ruby']['version']}-dev"
end

package 'libxml2-dev'
package 'libxslt1-dev'
package 'dos2unix'

execute 'remove_cr' do
  command 'dos2unix /etc/profile.d/Z99-ruby-gems.sh'
  action :nothing
end

cookbook_file "/etc/profile.d/Z99-ruby-gems.sh" do
  source 'Z99-ruby-gems.sh'
  owner 'root'
  group 'root'
  mode '0755'
  notifies :run, 'execute[remove_cr]', :delayed
  action :create
end

firewall_rule 'rails' do
  port 3000
  command :allow
end
