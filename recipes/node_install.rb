include_recipe 'apt'

package 'nodejs' do
  action :remove
end

package 'yarn' do
  action :remove
end

execute 'add nodejs key' do
  command 'curl -s https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add -'
end

apt_repository 'nodejs' do
  uri node['nodejs']['nodesource']
  distribution 'trusty'
  components ['main']
end

apt_repository 'yarn' do
  uri 'https://dl.yarnpkg.com/debian/'
  components ['stable', 'main']
  distribution ''
  key 'https://dl.yarnpkg.com/debian/pubkey.gpg'
end

apt_preference 'nodesource' do
  glob '*'
  pin  'origin deb.nodesource.com'
  pin_priority '700'
end

apt_preference 'yarnpkg' do
  glob '*'
  pin  'origin dl.yarnpkg.ccom'
  pin_priority '700'
end

package 'nodejs'
package 'yarn'

execute 'update npm' do
  command 'npm -g update npm'
end
