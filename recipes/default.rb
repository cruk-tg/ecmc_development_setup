#
# Cookbook Name:: ecmc_development_setup
# Recipe:: default
#
# Copyright 2018, University of Edinburgh
#
# All rights reserved - Do Not Redistribute
#
include_recipe 'apt'
package 'build-essential'
package 'bash-completion'
package 'git'
package 'unzip'
package 'MySql library install' do
  case node[:platform_version]
  when '18.04'
    package_name 'default-libmysqlclient-dev'
  when '16.04'
    package_name 'libmysqlclient-dev'
  when '14.04'
    package_name 'libmysqlclient-dev'
  end
end

package 'mysql-client'
package 'libgmp3-dev'

firewall 'default' do
  action :install
end

firewall_rule 'ssh' do
  port 22
  protocol :tcp
  command :allow
end

execute 'update-ca-certificates' do
  command 'update-ca-certificates'
  action :nothing
end

group 'docker' do
  members ['vagrant']
end

git "/usr/share/bash-git-prompt" do
  repository 'https://github.com/magicmonty/bash-git-prompt.git'
end

cookbook_file '/etc/profile.d/Z99-bash-git-prompt.sh' do
  source 'Z99-bash-git-prompt.sh'
  owner 'root'
  group 'root'
  mode  '0755'
  action :create
end

cookbook_file "/usr/local/share/ca-certificates/university_of_edinburgh_ca_2.crt" do
  source 'university_of_edinburgh_ca_2.crt'
  owner 'root'
  group 'root'
  mode '0755'
  action :create
  notifies :run, 'execute[update-ca-certificates]', :immediately
end

remote_file '/usr/local/bin/docker-compose' do
  source 'https://github.com/docker/compose/releases/download/1.21.2/docker-compose-Linux-x86_64'
  owner 'root'
  group 'root'
  mode  '0755'
  action :create
end
